/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras;

/**
 * Una clase para representar círculos situados sobre el plano
 * Cada círculo queda determinado por su radio junto con las coordenadas de su centro.
 * @version 1.2, 09/08/2017
 * @author Catalina Aguayo
 */
public class Circulo {
    
    protected double x,y; //coordenadas del centro
    protected double r; //radio del círculo
    
    /**
     * Crea un círculo a partir de su origen su radio.
     * @param x La coordenada x del centro del círculo.
     * @param y La coordenada y del centro del círculo.
     * @param r El radio del círculo, debe ser mayor o igual a 0.
     */
    public Circulo (double x, double y, double r){
        this.x=x; this.y=y; this.r=r;
    }
    
    /**
     * Cálculo del área de este círculo.
     * @return El área (mayor o igual que 0) del círculo.
     */
    public double area (){
        return Math.PI*r*r;
    }
    
    /**
     * Indica si un punto está adentro del círculo.
     * @param px Componente x del punto.
     * @param py Componente y del punto.
     * @return True si el punto está adentro del círculo, False en otro caso.
     */
    public boolean contiene(double px, double py){
        /**
         * Calculamos la distancia d del punto (px,py) al centro del círculo (x,y)
         * que se obtiene como la raíz cuadrada de (px-x)^2+(py-y)^2;
         */
        double d = Math.sqrt((px-x)*(px-x)+(py-y)*(py-y));
        // el círculo contiene el punto d si es menor o igual al radio.
        return d <= r;
    }
    
}
