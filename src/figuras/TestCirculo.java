/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras;

import java.util.Scanner;

/**
 *
 * @author catalinaaguayo
 */
public class TestCirculo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
     
        System.out.println("Ingrese valor coordenada x del centro del círculo");
        double x = leer();
        System.out.println("Ingrese valor coordenada y del centro del círculo");
        double y = leer();
        System.out.println("Ingrese valor del radio del círculo");
        double r = leer();  
        Circulo circulo = new Circulo(x,y,r);
        
        System.out.println("El área del círculo es "+circulo.area());

    }
    
    public static Double leer (){
        Scanner leer = new Scanner (System.in);
        Double valor = leer.nextDouble();
        return valor;
    }

    
}
